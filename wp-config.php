<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */
//auminhtu / NguyenThuTr@ng2001
// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wp_newsportal' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'regIJD/ 7+t1I~=8jQU.$oxi{:UckfC@kiBNvovM|mjJdV&1E!PmF4q8^yBO5gI5' );
define( 'SECURE_AUTH_KEY',  'e[$TAL)Z`Dn*J%ICbn|k*`.{N*? ,}-4u*LtnV|qm?4D/6/,0lgcR$DW<z4o,mzh' );
define( 'LOGGED_IN_KEY',    'w9%By}7iS,ufXZ3X?:ElfA4x?)9gNJ5/==}j}5M_jv:4+QR&w-?xWaN02aZfl4L~' );
define( 'NONCE_KEY',        ' E w}oJRK*Y#0UkV]-E3GBj~0|wZp.;XE.Mvj*}{PL6Zg1{9FS&o+lk7iDE#DvU3' );
define( 'AUTH_SALT',        '{n1bMs2mdsPf%7no~Uh#m{&NU$Z;%Q4w}|uVk5)~!yF)P#czP!B]E3A%` 3htz&L' );
define( 'SECURE_AUTH_SALT', 'gLCsquvJ13M]E@h#<8]5*K0N;Z%+<6~c+o#]x]EWoyghf#.NtBdM+pJyU+brh|_n' );
define( 'LOGGED_IN_SALT',   '-_R5m`FxKzg78&4:O@93LZ4FB/gWT N,<}l8Md`ky>&Q&yL/pTwAb`&d4B}ViZAK' );
define( 'NONCE_SALT',       '2UhVB3!}X*bGDNbm_GRr=b2qTj18;rq)aIPKy.mhC>u!OdU`qhfRc&;a4? ozkyX' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
